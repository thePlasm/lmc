#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

/* the array of instructions and data that is used by the lmc simulator, each value must be between 0 and 999 */
unsigned short mailboxes[100];

/* the program counter - the index of the instruction that is being executed */
unsigned char pc = 0;

/* the accumulator used for storing results of computations*/
unsigned short accumulator = 0;

/* is 1 if negative and 0 if positive, is updated by 1xx (ADD) and 2xx (SUB), and is used by 8xx (BRP) */
unsigned char nflag = 0;

/*
 * char initMailboxes(FILE *inFile)
 * inFile: file pointer to assembled lmc code file
 * return value: 1 upon error and 0 upon success
 * side effects: populates the mailboxes global variable with the values in the file and initialises the rest to 0
 */
char initMailboxes(FILE *inFile) {
	char tempLine[3];
	char lineIndex = 0;
	int curr = getc(inFile);
	int mailIndex;
	for (mailIndex = 0; mailIndex < 100; mailIndex++) {
		int i;
		while (isspace(curr))
			curr = getc(inFile);
		if (curr == EOF)
			break;
		for (i = 0; i < 3; i++) {
			if (curr < '0' || curr > '9')
				return 1;
			tempLine[i] = curr;
			curr = getc(inFile);
		}
		mailboxes[mailIndex] = 100 * (tempLine[0] - '0') + 10 * (tempLine[1] - '0') + (tempLine[2] - '0');
	}
	while (mailIndex < 100) {
		mailboxes[mailIndex] = 0;
		mailIndex++;
	}
	return 0;
}

/* 901 - stores the value in INBOX (stdin) in the accumulator */
void inp() {
	scanf("%3hu", &accumulator);
	if (accumulator > 999)
		accumulator = 0;
}

/* 902 - loads the value in the accumulator into the OUTBOX (stdout) */
void out() {
	printf("%03hu\n", accumulator);
}

/* 8xx - updates pc to num if the negative flag is not set */
void brp(unsigned char num) {
	if (!nflag)
		pc = num - 1;
}

/* 7xx - updates pc to num if the accumulator's value is 0 */
void brz(unsigned char num) {
	if (!accumulator)
		pc = num - 1;
}

/* 6xx - sets pc to num */
void bra(unsigned char num) {
	pc = num - 1;
}

/* 5xx - loads the value of mailboxes[num] into the accumulator */
void lda(unsigned char num) {
	accumulator = mailboxes[num];
}

/* 3xx - stores the value of the accumulator into mailboxes[num] */
void sta(unsigned char num) {
	mailboxes[num] = accumulator;
}

/* 2xx - subtracts the value of mailboxes[num] from the accumulator, updates nflag accordingly */
void sub(unsigned char num) {
	if (mailboxes[num] <= accumulator) {
		accumulator -= mailboxes[num];
		nflag = 0;
	}
	else
		nflag = 1;
}
/* 1xx - adds the value of mailboxes[num] to the accumulator, updates nflag accordingly */
void add(unsigned char num) {
	accumulator += mailboxes[num];
	if (accumulator > 999)
		accumulator = accumulator % 1000;
	else
		nflag = 0;
}

/* executes the appropriate instruction depending on the opcode in instruction */
void execInstruction(short instruction) {
	switch (instruction / 100) {
		case 0: {
			break;
		}
		case 1: {
			add(instruction % 100);
			break;
		}
		case 2: {
			sub(instruction % 100);
			break;
		}
		case 3: {
			sta(instruction % 100);
			break;
		}
		case 5: {
			lda(instruction % 100);
			break;
		}
		case 6: {
			bra(instruction % 100);
			break;
		}
		case 7: {
			brz(instruction % 100);
			break;
		}
		case 8: {
			brp(instruction % 100);
			break;
		}
		case 9: {
			if (instruction % 100 == 1)
				inp();
			else if (instruction % 100 == 2)
				out();
			break;
		}
	}
}

int main(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <filename>\n", *argv);
		return EXIT_FAILURE;
	}
	FILE *inFile = fopen(argv[1], "r");
	if (inFile == NULL) {
		perror("Error: ");
		return EXIT_FAILURE;
	}
	if (initMailboxes(inFile)) {
		fprintf(stderr, "Error: Invalid file %s.\n", argv[1]);
		return EXIT_FAILURE;
	}
	while (pc >= 0 && pc < 100 && mailboxes[pc] != 0) {
		execInstruction(mailboxes[pc]);
		pc++;
	}
        if (fclose(inFile) == EOF)
        {
                fprintf(stderr, "Error closing file: %s. \n", argv[1]);
                return EXIT_FAILURE;
        }
	return EXIT_SUCCESS;
}
