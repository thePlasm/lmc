# lmc

A little man computer interpreter written in C.

# build

cc lmc.c -o lmc

# usage

lmc <input file>

# TODO

The interpreter only supports files that have the numeric lmc code and not the mnemonic code. A compiler should be written to translate the mnemonic code to the numeric code.
